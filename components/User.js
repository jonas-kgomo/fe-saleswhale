import React from 'react';



const User = ({posts}) => (
 <div class="flex-inline  justify-center">
    { 
      posts.current_user.map(post => {
   
        
        return  (
       
         
                   
       <div key={post.id} class=" md:flex bg-white justify-end rounded-lg p-3">
                     <img class="h-5 w-5 md:h-10 md:w-10 rounded-full mx-auto md:mx-0 md:mr-6" src={post.avatar}/>
   
                     <h2 class="text-sm m-2">{post.name}</h2>

       </div>
                          )
         } )
     
}
  </div>
);
 
export default  User;